#!/usr/bin/env python

# import modules used here -- sys is a very standard one
import sys

class Checkout:
    def __init__(self, items, priceMap):
        self.items = items
        self.priceMap = priceMap
        self.offersMap = {}
        self.totalCost = 0

    def checkout(self):
        #global totalCost
        checkoutMap = {}
        for item in self.items:
            if item in checkoutMap:
                checkoutMap[item] += 1
            else:
                checkoutMap[item] = 1

            if not item in self.offersMap:
                self.offersMap[item] = 0

            self.totalCost += self.scanItem(item, self.priceMap)
            self.totalCost = self.checkOffers(checkoutMap, self.totalCost, self.priceMap)
        return self.total()

    def scanItem(self, itemCode, priceMap):
        return priceMap[itemCode]

    def checkOffers(self, currentCheckoutMap, currentCost, priceMap):
        if "A" in currentCheckoutMap and currentCheckoutMap["A"] % 3 == 0 and (currentCheckoutMap["A"] / 3) > self.offersMap["A"]:
            currentCost = currentCost - priceMap["A"]
            self.offersMap["A"] += 1
        elif "B" in currentCheckoutMap and currentCheckoutMap["B"] % 3 == 0 and (currentCheckoutMap["B"] / 3) > self.offersMap["B"]:
            currentCost = currentCost - 20
            self.offersMap["B"] += 1
        return currentCost

    def total(self):
        return self.totalCost

