#!/usr/bin/env python

import pos
import unittest

class Test_TestPos(unittest.TestCase):
    def test_posSpecTest(self):
        pos1 = pos.Checkout(['B', 'A', 'B', 'P', 'B'], { 'A': 25, 'B': 40, 'P': 30 })
        self.assertEqual(pos1.checkout(), 155)

    def test_posSpecTest2(self):
        pos2 = pos.Checkout(['A', 'A'], { 'A': 25, 'B': 40, 'P': 30 })
        self.assertEqual(pos2.checkout(), 50)

    def test_posSpecTest3(self):
        pos3 = pos.Checkout(['B', 'A', 'B', 'P', 'B', 'A'], { 'A': 25, 'B': 40, 'P': 30 })
        self.assertEqual(pos3.checkout(), 180)

    def test_posSpecTest4(self):
        pos4 = pos.Checkout(['B', 'A', 'B', 'P', 'B', 'A', 'P', 'B', 'B', 'B'], { 'A': 25, 'B': 40, 'P': 30 })
        self.assertEqual(pos4.checkout(), 310)

    def test_posSpecTest5(self):
        pos5 = pos.Checkout(['A', 'A', 'A', 'A', 'A'], { 'A': 25, 'B': 40, 'P': 30 })
        self.assertEqual(pos5.checkout(), 100)

    def test_posSpecTest6(self):
        pos6 = pos.Checkout(['A', 'A'], { 'A': 25, 'B': 40, 'P': 30 })
        self.assertEqual(pos6.checkout(), 50)

    def test_posSpecTest7(self):
        pos7 = pos.Checkout(['A', 'A', 'A'], { 'A': 25, 'B': 40, 'P': 30 })
        self.assertEqual(pos7.checkout(), 50)

    def test_posSpecTest8(self):
        pos8 = pos.Checkout(['A', 'A', 'A', 'A', 'A', 'A', 'A', 'A'], { 'A': 25, 'B': 40, 'P': 30 })
        self.assertEqual(pos8.checkout(), 150)

    def test_posSpecTest9(self):
        pos9 = pos.Checkout(['A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A'], { 'A': 25, 'B': 40, 'P': 30 })
        self.assertEqual(pos9.checkout(), 150)
       
    def test_posSpecTest10(self):
        pos10 = pos.Checkout(['B', 'B', 'B'], { 'A': 25, 'B': 40, 'P': 30 })
        self.assertEqual(pos10.checkout(), 100)

    def test_posSpecTest11(self):
        pos11 = pos.Checkout(['B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B'], { 'A': 25, 'B': 40, 'P': 30 })
        self.assertEqual(pos11.checkout(), 400)

    def test_posSpecTest12(self):
        pos12 = pos.Checkout(['B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B'], { 'A': 25, 'B': 40, 'P': 30 })
        self.assertEqual(pos12.checkout(), 440)

    def test_posSpecTest13(self):
        pos13 = pos.Checkout(['B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B'], { 'A': 25, 'B': 40, 'P': 30 })
        self.assertEqual(pos13.checkout(), 480)

    def test_posSpecTest14(self):
        pos14 = pos.Checkout(['B', 'B', 'A', 'P', 'P', 'B', 'A', 'A', 'P', 'A'], { 'A': 25, 'B': 40, 'P': 30 })
        self.assertEqual(pos14.checkout(), 265)

    def test_posSpecTest15(self):
        pos15 = pos.Checkout(['B', 'B', 'A', 'P', 'P', 'B', 'A', 'A', 'P'], { 'A': 25, 'B': 40, 'P': 30 })
        self.assertEqual(pos15.checkout(), 240)

if __name__ == '__main__':
    unittest.main()